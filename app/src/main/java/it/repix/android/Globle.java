package it.repix.android;

import android.graphics.Bitmap;
import android.os.Environment;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;

public class Globle {


    public static ArrayList<String> IMAGEALLARY = new ArrayList<>();
    public static String package_name = "https://play.google.com/store/apps/details?id=";
    public static final String GoogleDrive = "https://drive.google.com/uc?view=download&id=";
    public static String app_link = "https://play.google.com/store/apps/details?id=";

    public static void listAllImages(File filepath) {
        File[] files = filepath.listFiles();
        if (files != null) {
            for (int j = files.length - 1; j >= 0; j--) {
                String ss = files[j].toString();
                File check = new File(ss);
                Log.d("" + check.length(), "" + check.length());
                if (check.length() <= PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID) {
                    Log.i("Invalid Image", "Delete Image");
                } else if (check.toString().contains(".jpg") || check.toString().contains(".png") || check.toString().contains(".jpeg")) {
                    IMAGEALLARY.add(ss);
                }
                System.out.println(ss);
            }
            return;
        }
        System.out.println("Empty Folder");
    }
}
