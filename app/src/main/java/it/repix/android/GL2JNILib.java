package it.repix.android;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;

public class GL2JNILib {
    public static final int DEVICE_IPAD_1X = 1;
    public static final int DEVICE_IPAD_2X = 2;
    public static final int DEVICE_IPHONE_2X = 0;
    public static final int DEVICE_IPHONE_3X = 3;
    public static final String TAG = "repix";

    public static native boolean canRedo();

    public static native void clear(int[] iArr, int i, int i2);

    public static native boolean closeStore();

    public static native void didReceiveMemoryWarning();

    public static native Bitmap getProcessedPhoto();

    private static native void init(int i, int i2, int i3);

    public static native void openStore();

   // public static native void productRequestComplete(ProductResponse[] productResponseArr, int i, int i2);

    public static native void purchaseCompleted(String str, int i);

    public static native void redo();

    public static native void resetToOriginal();

    public static native int step();

    public static native void undo();

    public static native void updatePurchasedFlags();

    static {
        System.loadLibrary(TAG);
    }

    public static void init(int width, int height) {
        init(width, height, getDeviceType());
    }

    public static int getDeviceType() {
        Resources res = RepixActivity.getInstance().getResources();
        return DEVICE_IPHONE_3X;

//        if (res.getDisplayMetrics().density <= 1.5f) {
//            return DEVICE_IPAD_1X;
//        }
//        boolean isXxHdpi;
//        boolean isLarge = (res.getConfiguration().screenLayout & 15) >= DEVICE_IPHONE_3X;
//        DisplayMetrics metrics = new DisplayMetrics();
//        RepixActivity.getInstance().getWindowManager().getDefaultDisplay().getMetrics(metrics);
//        if (metrics.densityDpi > 320) {
//            isXxHdpi = true;
//        } else {
//            isXxHdpi = false;
//        }
//        if (isLarge) {
//            return DEVICE_IPAD_2X;
//        }
//        if (isXxHdpi) {
//            return DEVICE_IPHONE_3X;
//        }
//        return DEVICE_IPHONE_2X;
    }
}
