package it.repix.android;

import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdRequest;

public class FBAds {

    public static AdView adView;
    public static InterstitialAd fbInterstitialAd;
    public static com.google.android.gms.ads.AdView googleAdView;
    public static com.google.android.gms.ads.InterstitialAd googleInterstitialAd;

    public static AdView GetFbAdView(final Context context,int margin) {
        adView = new AdView(context, context.getString(R.string.fb_banner), AdSize.BANNER_HEIGHT_50);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.topMargin = margin;
        adView.setBackgroundColor(context.getResources().getColor(R.color.white));
        adView.setLayoutParams(params);
        adView.loadAd();
        adView.setAdListener(new AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                Log.d("FBAds", adError.getErrorMessage());
                adView.destroy();
                getGoogleAdView(context, 0);
            }

            @Override
            public void onAdLoaded(Ad ad) {
                Log.d("FBAds", "loaded");
            }

            @Override
            public void onAdClicked(Ad ad) {
                Log.d("FBAds", "clicked");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                Log.d("FBAds", "onLoggingImpression");
            }
        });
        return adView;
    }

    public static com.google.android.gms.ads.AdView getGoogleAdView(final Context context, int margin) {
        googleAdView = new com.google.android.gms.ads.AdView(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.topMargin = margin;
        googleAdView.setLayoutParams(layoutParams);
        googleAdView.setAdSize(com.google.android.gms.ads.AdSize.BANNER);
        googleAdView.setAdUnitId(context.getString(R.string.admob_banner));
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(context.getString(R.string.test_device))
                .build();

        googleAdView.loadAd(adRequest);
        googleAdView.setAdListener(new com.google.android.gms.ads.AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {

                new android.os.Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        GetFbAdView(context,0);
                    }
                }, 1);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.d("interstialad code = ", String.valueOf(errorCode));
            }

            @Override
            public void onAdLeftApplication() {
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();

            }
        });

        return googleAdView;
    }

    public static AdView GetAdViewMedium(final Context context) {
        adView = new AdView(context, context.getString(R.string.fb_medium), AdSize.RECTANGLE_HEIGHT_250);
        adView.loadAd();
        adView.setAdListener(new AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                Log.d("FBAds", adError.getErrorMessage());
                adView.destroy();
                getGoogleAdView(context, 0);
            }

            @Override
            public void onAdLoaded(Ad ad) {
                Log.d("FBAds", "loaded");
            }

            @Override
            public void onAdClicked(Ad ad) {
                Log.d("FBAds", "clicked");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                Log.d("FBAds", "onLoggingImpression");
            }

        });
        return adView;
    }

    public static void showFbInterstitial(Context context) {

        if (fbInterstitialAd == null) {
            LoadFbInterstitialAd(context);
            return;
        }

        if (fbInterstitialAd.isAdLoaded()) {
            fbInterstitialAd.show();
            LoadFbInterstitialAd(context);
        } else if (googleInterstitialAd.isLoaded()) {
            googleInterstitialAd.show();
            LoadGoogleInterstitialAds(context);
        }
    }

    public static void showGoogleInterstitial(Context context) {

        if (googleInterstitialAd == null) {
            LoadGoogleInterstitialAds(context);
            return;
        }

        if (googleInterstitialAd.isLoaded()) {
            googleInterstitialAd.show();
            LoadGoogleInterstitialAds(context);
        }
    }

    public static void LoadFbInterstitialAd(Context context) {
        fbInterstitialAd = new InterstitialAd(context, context.getString(R.string.fb_interstitial));
        fbInterstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                Log.d("FBAds", "display");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                DBLog.LOGD("FBAds", "dismiss");

            }

            @Override
            public void onError(Ad ad, AdError adError) {
                DBLog.LOGD("FBAds", adError.getErrorMessage());

            }

            @Override
            public void onAdLoaded(Ad ad) {
                DBLog.LOGD("FBAds", "loaded");

            }

            @Override
            public void onAdClicked(Ad ad) {
                DBLog.LOGD("FBAds", "adclick");

            }

            @Override
            public void onLoggingImpression(Ad ad) {
                DBLog.LOGD("FBAds", "logging");

            }
        });
        fbInterstitialAd.loadAd();
    }

    public static void LoadGoogleInterstitialAds(final Context context) {
        googleInterstitialAd = new com.google.android.gms.ads.InterstitialAd(context);
        googleInterstitialAd.setAdUnitId(context.getString(R.string.admob_interstitial));

        AdRequest adRequest = new AdRequest.Builder()
                .build();

        googleInterstitialAd.loadAd(adRequest);
        googleInterstitialAd.setAdListener(new com.google.android.gms.ads.AdListener() {
            public void onAdLoaded() {
                DBLog.LOGD(" google interstialad", "Loaded");

            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                DBLog.LOGD(" google interstialad ", String.valueOf(i));
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });
    }
}
