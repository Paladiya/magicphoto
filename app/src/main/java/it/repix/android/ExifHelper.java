package it.repix.android;

import android.media.ExifInterface;

import java.io.IOException;
import java.util.HashMap;

public class ExifHelper {
    public static final String[] EXIF_KEYS = new String[]{"FNumber", "DateTime", "ExposureTime", "Flash", "FocalLength", "GPSAltitude", "GPSAltitudeRef", "GPSDateStamp", "GPSLatitude", "GPSLatitudeRef", "GPSLongitude", "GPSLongitudeRef", "GPSProcessingMethod", "GPSTimeStamp", "ImageLength", "ImageWidth", "ISOSpeedRatings", "Make", "Model", "Orientation", "WhiteBalance"};
    HashMap<String, String> map;

    public ExifHelper() {
        reset();
    }

    public void reset() {
        this.map = new HashMap();
    }

    public String getAttribute(String key) {
        return (String) this.map.get(key);
    }

    public void setAttribute(String key, String value) {
        this.map.put(key, value);
    }

    public void readExif(String filename) throws IOException {
        ExifInterface src = new ExifInterface(filename);
        for (String exifKey : EXIF_KEYS) {
            String exifValue = src.getAttribute(exifKey);
            if (exifValue != null) {
                this.map.put(exifKey, exifValue);
            }
        }
    }

    public void writeExif(String filename) throws IOException {
        ExifInterface target = new ExifInterface(filename);
        for (String exifKey : this.map.keySet()) {
            String exifValue = (String) this.map.get(exifKey);
            if (!(exifKey == null || exifValue == null)) {
                target.setAttribute(exifKey, exifValue);
            }
        }
        target.saveAttributes();
    }
}
