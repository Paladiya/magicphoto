package it.repix.android;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Process;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.View;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;

import it.repix.android.TestView;

public  class GL2JNIView extends GLSurfaceView {
    private static final boolean DEBUG = true;
    static final int HOVER_BEGIN = 6;
    static final int HOVER_END = 8;
    static final int HOVER_MOVE = 7;
    static final int PAN = 4;
    static final int PINCH = 5;
    private static String TAG = TestView.TAG;
    static final int TOUCH_BEGIN = 0;
    static final int TOUCH_CANCEL = 3;
    static final int TOUCH_END = 2;
    static final int TOUCH_MOVE = 1;
    static boolean inited = false;
    int deny = TOUCH_BEGIN;
    int mask = TOUCH_BEGIN;
    ScaleGestureDetector pinch;

    private static class ConfigChooser implements EGLConfigChooser {
        private static int EGL_OPENGL_ES2_BIT = GL2JNIView.PAN;
        private static int[] s_configAttribs2 = new int[]{12321, GL2JNIView.HOVER_END, 12324, GL2JNIView.HOVER_END, 12323, GL2JNIView.HOVER_END, 12322, GL2JNIView.HOVER_END, 12352, EGL_OPENGL_ES2_BIT, 12344};
        protected int mAlphaSize;
        protected int mBlueSize;
        protected int mDepthSize;
        protected int mGreenSize;
        protected int mRedSize;
        protected int mStencilSize;
        private int[] mValue = new int[GL2JNIView.TOUCH_MOVE];

        public ConfigChooser(int r, int g, int b, int a, int depth, int stencil) {
            this.mRedSize = r;
            this.mGreenSize = g;
            this.mBlueSize = b;
            this.mAlphaSize = a;
            this.mDepthSize = depth;
            this.mStencilSize = stencil;
        }

        public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display) {
            int[] num_config = new int[GL2JNIView.TOUCH_MOVE];
            egl.eglChooseConfig(display, s_configAttribs2, null, GL2JNIView.TOUCH_BEGIN, num_config);
            int numConfigs = num_config[GL2JNIView.TOUCH_BEGIN];
            Log.d(GL2JNIView.TAG, "chooseConfig numConfigs " + numConfigs);
            if (numConfigs <= 0) {
                RepixActivity.getInstance().alert("Failed to create suitable EGL configuration", new Dialog.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RepixActivity.getInstance().finish();
                        Process.killProcess(Process.myPid());
                    }
                });
                synchronized (this) {
                    try {
                        wait();
                    } catch (Exception e) {
                    }
                }
                throw new IllegalArgumentException("No configs match configSpec");
            }
            EGLConfig[] configs = new EGLConfig[numConfigs];
            egl.eglChooseConfig(display, s_configAttribs2, configs, numConfigs, num_config);
            printConfigs(egl, display, configs);
            return chooseConfig(egl, display, configs);
        }

        public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display, EGLConfig[] configs) {
            int length = configs.length;
            for (int i = GL2JNIView.TOUCH_BEGIN; i < length; i += GL2JNIView.TOUCH_MOVE) {
                EGLConfig config = configs[i];
                int d = findConfigAttrib(egl, display, config, 12325, GL2JNIView.TOUCH_BEGIN);
                int s = findConfigAttrib(egl, display, config, 12326, GL2JNIView.TOUCH_BEGIN);
                if (d >= this.mDepthSize && s >= this.mStencilSize) {
                    int r = findConfigAttrib(egl, display, config, 12324, GL2JNIView.TOUCH_BEGIN);
                    int g = findConfigAttrib(egl, display, config, 12323, GL2JNIView.TOUCH_BEGIN);
                    int b = findConfigAttrib(egl, display, config, 12322, GL2JNIView.TOUCH_BEGIN);
                    int a = findConfigAttrib(egl, display, config, 12321, GL2JNIView.TOUCH_BEGIN);
                    if (r == this.mRedSize && g == this.mGreenSize && b == this.mBlueSize && a == this.mAlphaSize) {
                        return config;
                    }
                }
            }
            return configs[GL2JNIView.TOUCH_BEGIN];
        }

        private int findConfigAttrib(EGL10 egl, EGLDisplay display, EGLConfig config, int attribute, int defaultValue) {
            if (egl.eglGetConfigAttrib(display, config, attribute, this.mValue)) {
                return this.mValue[GL2JNIView.TOUCH_BEGIN];
            }
            return defaultValue;
        }

        private void printConfigs(EGL10 egl, EGLDisplay display, EGLConfig[] configs) {
            int numConfigs = configs.length;
            String access$300 = GL2JNIView.TAG;
            Object[] objArr = new Object[GL2JNIView.TOUCH_MOVE];
            objArr[GL2JNIView.TOUCH_BEGIN] = Integer.valueOf(numConfigs);
            Log.w(access$300, String.format("%d configurations", objArr));
            for (int i = GL2JNIView.TOUCH_BEGIN; i < numConfigs; i += GL2JNIView.TOUCH_MOVE) {
                access$300 = GL2JNIView.TAG;
                objArr = new Object[GL2JNIView.TOUCH_MOVE];
                objArr[GL2JNIView.TOUCH_BEGIN] = Integer.valueOf(i);
                Log.w(access$300, String.format("Configuration %d:\n", objArr));
                printConfig(egl, display, configs[i]);
            }
        }

        private void printConfig(EGL10 egl, EGLDisplay display, EGLConfig config) {
            int[] attributes = new int[]{12320, 12321, 12322, 12323, 12324, 12325, 12326, 12327, 12328, 12329, 12330, 12331, 12332, 12333, 12334, 12335, 12336, 12337, 12338, 12339, 12340, 12343, 12342, 12341, 12345, 12346, 12347, 12348, 12349, 12350, 12351, 12352, 12354};
            String[] names = new String[]{"EGL_BUFFER_SIZE", "EGL_ALPHA_SIZE", "EGL_BLUE_SIZE", "EGL_GREEN_SIZE", "EGL_RED_SIZE", "EGL_DEPTH_SIZE", "EGL_STENCIL_SIZE", "EGL_CONFIG_CAVEAT", "EGL_CONFIG_ID", "EGL_LEVEL", "EGL_MAX_PBUFFER_HEIGHT", "EGL_MAX_PBUFFER_PIXELS", "EGL_MAX_PBUFFER_WIDTH", "EGL_NATIVE_RENDERABLE", "EGL_NATIVE_VISUAL_ID", "EGL_NATIVE_VISUAL_TYPE", "EGL_PRESERVED_RESOURCES", "EGL_SAMPLES", "EGL_SAMPLE_BUFFERS", "EGL_SURFACE_TYPE", "EGL_TRANSPARENT_TYPE", "EGL_TRANSPARENT_RED_VALUE", "EGL_TRANSPARENT_GREEN_VALUE", "EGL_TRANSPARENT_BLUE_VALUE", "EGL_BIND_TO_TEXTURE_RGB", "EGL_BIND_TO_TEXTURE_RGBA", "EGL_MIN_SWAP_INTERVAL", "EGL_MAX_SWAP_INTERVAL", "EGL_LUMINANCE_SIZE", "EGL_ALPHA_MASK_SIZE", "EGL_COLOR_BUFFER_TYPE", "EGL_RENDERABLE_TYPE", "EGL_CONFORMANT"};
            int[] value = new int[GL2JNIView.TOUCH_MOVE];
            for (int i = GL2JNIView.TOUCH_BEGIN; i < attributes.length; i += GL2JNIView.TOUCH_MOVE) {
                int attribute = attributes[i];
                String name = names[i];
                if (egl.eglGetConfigAttrib(display, config, attribute, value)) {
                    String access$300 = GL2JNIView.TAG;
                    Object[] objArr = new Object[GL2JNIView.TOUCH_END];
                    objArr[GL2JNIView.TOUCH_BEGIN] = name;
                    objArr[GL2JNIView.TOUCH_MOVE] = Integer.valueOf(value[GL2JNIView.TOUCH_BEGIN]);
                    Log.w(access$300, String.format("  %s: %d\n", objArr));
                } else {
                    while (egl.eglGetError() != 12288) {
                    }
                }
            }
        }
    }

    private static class ContextFactory implements EGLContextFactory {
        private static int EGL_CONTEXT_CLIENT_VERSION = 12440;
        static EGLContext context = null;

        private ContextFactory() {
        }

        public EGLContext createContext(EGL10 egl, EGLDisplay display, EGLConfig eglConfig) {
            if (context == null) {
                Log.w(GL2JNIView.TAG, "creating OpenGL ES 2.0 context");
                GL2JNIView.checkEglError("Before eglCreateContext", egl);
                int[] attrib_list = new int[GL2JNIView.TOUCH_CANCEL];
                attrib_list[GL2JNIView.TOUCH_BEGIN] = EGL_CONTEXT_CLIENT_VERSION;
                attrib_list[GL2JNIView.TOUCH_MOVE] = GL2JNIView.TOUCH_END;
                attrib_list[GL2JNIView.TOUCH_END] = 12344;
                context = egl.eglCreateContext(display, eglConfig, EGL10.EGL_NO_CONTEXT, attrib_list);
                GL2JNIView.checkEglError("After eglCreateContext", egl);
            }
            Log.w(GL2JNIView.TAG, "createContext = " + context);
            return context;
        }

        public void destroyContext(EGL10 egl, EGLDisplay display, EGLContext context) {
            Log.d("TAG", "destroyContext " + context);
        }
    }

    private class Renderer implements GLSurfaceView.Renderer {
        int frames;

        private Renderer() {
            this.frames = GL2JNIView.TOUCH_BEGIN;
        }

        public void onDrawFrame(GL10 gl) {
            if (RepixActivity.pendingBitmap != null) {
                GL2JNILib.clear(null, GL2JNIView.TOUCH_BEGIN, GL2JNIView.TOUCH_BEGIN);
                final Bitmap bmp = RepixActivity.pendingBitmap;
                RepixActivity.pendingBitmap = null;
                GL2JNIView.this.queueEvent(new Runnable() {
                    public void run() {
                        GL2JNIView.this.setPhoto(bmp);
                        bmp.recycle();
                        System.gc();
                    }
                });
            }
            if (GL2JNILib.step() != 0 || this.frames < 10) {
                GL2JNIView.this.requestRender();
            }
            if (this.frames == 0) {
                RepixActivity.getInstance().openEditor();
            }
            if (this.frames == GL2JNIView.TOUCH_MOVE) {
                RepixActivity.getInstance().hideSplash();
            }
            RepixActivity.getInstance().setRedoEnabled(GL2JNILib.canRedo());
            this.frames += GL2JNIView.TOUCH_MOVE;
        }

        public void onSurfaceChanged(GL10 gl, int width, int height) {
            Log.d(GL2JNIView.TAG, "onSurfaceChanged " + width + ", " + height);
            GL2JNILib.init(width, height);
            GLES20.glViewport(GL2JNIView.TOUCH_BEGIN, GL2JNIView.TOUCH_BEGIN, width, height);
            GLES20.glDisable(3024);
            this.frames = GL2JNIView.TOUCH_BEGIN;
        }

        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
            Log.d(GL2JNIView.TAG, "onSurfaceCreated " + Thread.currentThread());
        }
    }

    private class ScaleListener extends SimpleOnScaleGestureListener {
        float x;
        float y;

        private ScaleListener() {
        }

        public void onScaleEnd(ScaleGestureDetector detector) {
            this.y = 0.0f;
            this.x = 0.0f;
        }

        public boolean onScaleBegin(ScaleGestureDetector detector) {
            this.x = detector.getFocusX();
            this.y = detector.getFocusY();
            return super.onScaleBegin(detector);
        }

        public boolean onScale(ScaleGestureDetector detector) {
            float s = detector.getScaleFactor();
            GL2JNIView.this.xtouchEvent(GL2JNIView.PINCH, GL2JNIView.TOUCH_BEGIN, GL2JNIView.TOUCH_BEGIN, detector.getFocusX(), detector.getFocusY(), s);
            GL2JNIView.this.xtouchEvent(GL2JNIView.PAN, GL2JNIView.TOUCH_BEGIN, GL2JNIView.TOUCH_BEGIN, detector.getFocusX() - this.x, detector.getFocusY() - this.y, 0.0f);
            this.x = detector.getFocusX();
            this.y = detector.getFocusY();
            return GL2JNIView.DEBUG;
        }
    }

    protected native void touchEvent(int i, int i2, int i3, float f, float f2, float f3);

    public GL2JNIView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setPreserveEGLContextOnPause(DEBUG);
        init(false, TOUCH_BEGIN, TOUCH_BEGIN);
        this.pinch = new ScaleGestureDetector(context, new ScaleListener());
        setOnHoverListener(new OnHoverListener() {
            public boolean onHover(View view, MotionEvent motionEvent) {
                GL2JNIView.this.onHoverEvent(motionEvent);
                return false;
            }
        });
    }

    void setPhoto(Bitmap bmp) {
        Log.d(TAG, "view setPhoto " + bmp);
        final int w = bmp.getWidth();
        final int h = bmp.getHeight();
        final int[] pixels = new int[(w * h)];
        bmp.getPixels(pixels, TOUCH_BEGIN, w, TOUCH_BEGIN, TOUCH_BEGIN, w, h);
        queueEvent(new Runnable() {
            public void run() {
                Log.i("sumo", "queueEvent clear");
                GL2JNILib.clear(pixels, w, h);
            }
        });
    }

    private void init(boolean translucent, int depth, int stencil) {
        Log.d("TAG", "GL init " + translucent);
        setDebugFlags(TOUCH_CANCEL);
        setEGLContextFactory(new ContextFactory());
        setEGLConfigChooser(translucent ? new ConfigChooser(HOVER_END, HOVER_END, HOVER_END, HOVER_END, depth, stencil) : new ConfigChooser(HOVER_END, HOVER_END, HOVER_END, TOUCH_BEGIN, depth, stencil));
        setRenderer(new Renderer());
        setRenderMode(TOUCH_BEGIN);
    }

    public boolean onHoverEvent(MotionEvent event) {
        int tooltype = event.getToolType(TOUCH_BEGIN);
        Log.d(TAG, "ZZZZ onHoverEvent " + event);
        switch (event.getAction()) {
            case HOVER_MOVE /*7*/:
                xtouchEvent(HOVER_MOVE, TOUCH_BEGIN, tooltype, event.getX(), event.getY(), 0.0f);
                break;
           /* case R.styleable.MenuDrawer_mdAllowIndicatorAnimation *//*9*//*:
                xtouchEvent(HOVER_BEGIN, TOUCH_BEGIN, tooltype, event.getX(), event.getY(), 0.0f);
                break;
            case R.styleable.MenuDrawer_mdMaxAnimationDuration *//*10*//*:
                xtouchEvent(HOVER_END, TOUCH_BEGIN, tooltype, event.getX(), event.getY(), 0.0f);
                break;*/
        }
        requestRender();
        return super.onHoverEvent(event);
    }

    protected void xtouchEvent(int type, int index, int tool, float x, float y, float z) {
        if (type == PAN || type == PINCH) {
            if (this.deny == 0) {
                xtouchEvent(TOUCH_CANCEL, index, tool, x, y, z);
            }
            this.deny |= HOVER_BEGIN;
        }
        if ((this.deny & (TOUCH_MOVE << type)) == 0) {
            final int i = type;
            final int i2 = index;
            final int i3 = tool;
            final float f = x;
            final float f2 = y;
            final float f3 = z;
            queueEvent(new Runnable() {
                public void run() {
                    GL2JNIView.this.touchEvent(i, i2, i3, f, f2, f3);
                    GL2JNIView.this.requestRender();
                }
            });
        }
        if (type == TOUCH_END) {
            this.deny = TOUCH_BEGIN;
            this.mask = TOUCH_BEGIN;
        }
        this.mask |= TOUCH_MOVE << type;
    }



    float clamp(float v) {
        if (v > 1.0f) {
            return 1.0f;
        }
        if (v < 0.0f) {
            return 0.0f;
        }
        return v;
    }

    public boolean onTouchEvent(MotionEvent event) {
        this.pinch.onTouchEvent(event);
        int tooltype = event.getToolType(TOUCH_BEGIN);
        float pressure = event.getPressure();
        if (tooltype == TOUCH_MOVE) {
            pressure *= 6.0f;
        }
        if (tooltype == TOUCH_END) {
            pressure = ((float) (0.05d + Math.pow((double) pressure, 0.75d))) * 1.33f;
        }
        pressure = clamp(pressure);
        switch (event.getAction()) {
            case TOUCH_BEGIN /*0*/:
                xtouchEvent(TOUCH_BEGIN, TOUCH_BEGIN, tooltype, event.getX(), event.getY(), pressure);
                break;
            case TOUCH_MOVE /*1*/:
                xtouchEvent(TOUCH_END, TOUCH_BEGIN, tooltype, event.getX(), event.getY(), pressure);
                break;
            case TOUCH_END /*2*/:
                xtouchEvent(TOUCH_MOVE, TOUCH_BEGIN, tooltype, event.getX(), event.getY(), pressure);
                break;
            case TOUCH_CANCEL /*3*/:
                xtouchEvent(TOUCH_CANCEL, TOUCH_BEGIN, tooltype, event.getX(), event.getY(), pressure);
                break;
        }
        return DEBUG;
    }

    private static void checkEglError(String prompt, EGL10 egl) {
        while (true) {
            int error = egl.eglGetError();
            if (error != 12288) {
                String str = TAG;
                Object[] objArr = new Object[TOUCH_END];
                objArr[TOUCH_BEGIN] = prompt;
                objArr[TOUCH_MOVE] = Integer.valueOf(error);
                Log.e(str, String.format("%s: EGL error: 0x%x", objArr));
            } else {
                return;
            }
        }
    }
}
