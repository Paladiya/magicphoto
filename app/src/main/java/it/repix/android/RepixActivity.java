package it.repix.android;

import android.Manifest;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore.Images.Media;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import it.repix.android.Adapter.AdsAdapter;


public class RepixActivity extends AppCompatActivity {

    private static final int STORAGE_PERMISSION_REQUEST = 1;
    public static final boolean DEBUG = false;
    public static final int RC_CAMERA = 40002;
    public static final int RC_GALLERY = 40001;
    public static final String TAG = "repix";
    public static final String URL_REPIX_FEED = "http://repix.it/android/feed";
    public static final String URL_REPIX_HELP = "http://repix.it/android150/help";
    static final String[] DEFAULT_IMAGES = new String[]{"/assets/default/helsinki.jpg", "/assets/default/000001806840.jpg", "/assets/default/137461279.jpg"};
    public static ArrayList<Category> categories;
    static GL2JNIView glView;
    static RepixActivity instance;
    static Bitmap pendingBitmap = null;
    Dialog dialogExit;
    String currentToolbar;
    ExifHelper exif = new ExifHelper();
    boolean glViewHack = DEBUG;
    MenuItem menuRedo;
    MenuItem menuReset;
    MenuItem menuSave;
    MenuItem menuShareMore;
    MenuItem menuStoreClose;
    MenuItem menuUndo;
    boolean redoState = true;
    Handler restoreTimer = new Handler();
    Runnable restoreTimerRunnable = new Runnable() {
        public void run() {
            RepixActivity.this.restoreTimer.postDelayed(this, 300000);
        }
    };
    HashMap<MenuItem, ResolveInfo> shareInfo;
    ViewGroup webviewlayout;
    DrawerLayout drawer;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    int isBackPressed = 0;
    LinearLayout linearLayout, linearLayoutAds;
    RecyclerView recyclerViewAds;
    String resultJson;
    private TextView yes, no;
    private int mInterval = 60000 * 3;
    private Handler mHandler;
    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                FBAds.showFbInterstitial(RepixActivity.this);
            } finally {
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    static RepixActivity getInstance() {
        return instance;
    }

    public void queueEvent(Runnable r) {
        if (glView != null) {
            glView.queueEvent(r);
        }
    }

    public void confirm(final String message, final OnClickListener positiveListener) {
        runOnUiThread(new Runnable() {
            public void run() {
                new Builder(RepixActivity.this).setMessage(message).setPositiveButton("OK", positiveListener).setNegativeButton("Cancel", null).show();
            }
        });
    }

    public void alert(final String message, final OnClickListener positiveListener) {
        runOnUiThread(new Runnable() {
            public void run() {
                new Builder(RepixActivity.this).setMessage(message).setPositiveButton("OK", positiveListener).show();
            }
        });
    }

    public void onLowMemory() {
        cleanupMemory();
    }

    public void onTrimMemory(int level) {
        cleanupMemory();
    }

    private void cleanupMemory() {
        queueEvent(new Runnable() {
            public void run() {
                GL2JNILib.didReceiveMemoryWarning();
            }
        });
    }

    String getContentFilename(Uri contentUri) throws IOException {
        if (contentUri == null) {
            return null;
        }
        if ("file".equals(contentUri.getScheme())) {
            return contentUri.getPath();
        }
        Cursor cursor = getContentResolver().query(contentUri, new String[]{"_data"}, null, null, null);
        if (cursor == null) {
            return null;
        }
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    String currentTimeAsExif() {
        return new SimpleDateFormat("yyyy:MM:dd HH:mm:ss").format(new Date());
    }

    void saveExif(Uri contentUri) throws IOException {
        String filename = getContentFilename(contentUri);
        if (filename != null) {
            String versionString = null;
            try {
                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                if (pInfo != null) {
                    versionString = pInfo.versionName;
                }
            } catch (NameNotFoundException e) {
            }
            if (versionString == null) {
                versionString = "0.0";
            }
            this.exif.setAttribute("DateTime", currentTimeAsExif());
            this.exif.setAttribute("Orientation", "0");
            this.exif.setAttribute("Software", "Repix " + versionString + " (Android)");
            this.exif.setAttribute("Description", "Made with Repix (http://repix.it)");
            this.exif.setAttribute("ImageLength", null);
            this.exif.setAttribute("ImageWidth", null);
            this.exif.setAttribute("ImageHeight", null);
            this.exif.writeExif(filename);
        }
    }

    public void sharePhoto(final ResolveInfo info) {
        FBAds.showFbInterstitial(this);
        final boolean saveOnly = (info == null || info.activityInfo == null || "it.repix.android".equals(info.activityInfo.packageName)) ? true : DEBUG;
        if (Environment.getExternalStorageState().equals("mounted")) {
            queueEvent(new Runnable() {
                public void run() {
                    try {
                        final Bitmap photo = GL2JNILib.getProcessedPhoto();
                        RepixActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                try {
                                    final String uri = RepixActivity.this.savePhoto(photo);
                                    if (saveOnly) {
                                        Toast.makeText(RepixActivity.this, R.string.saveToCollection, Toast.LENGTH_SHORT).show();
                                        drawer.openDrawer(Gravity.START);
                                        return;
                                    }
                                    final ComponentName name = new ComponentName(info.activityInfo.applicationInfo.packageName, info.activityInfo.name);
                                    Toast.makeText(RepixActivity.this, "Opening share application...", Toast.LENGTH_SHORT).show();
                                    new Handler().postAtTime(new Runnable() {
                                        public void run() {
                                            Intent share = new Intent("android.intent.action.SEND");
                                            share.setComponent(name);
                                            share.setType("image/jpeg");
                                            share.putExtra("android.intent.extra.STREAM", Uri.parse(uri));
                                            share.putExtra("android.intent.extra.SUBJECT", "Made with Repix - http://repix.it ");
                                            share.putExtra("android.intent.extra.TEXT", "#repix ");
                                            RepixActivity.this.startActivity(share);
                                        }
                                    }, 50);
                                } catch (IOException e) {
                                    Toast.makeText(RepixActivity.this, "Could not save: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    } catch (Exception e) {
                    }
                }
            });
        } else {
            alert("Cannot save, external storage is not available", null);
        }
    }

    private String savePhoto(Bitmap photo) throws IOException {
        String filename = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".jpg";
        File repixDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Repix");
        repixDir.mkdirs();
        File destPath = new File(repixDir, filename);
        FileOutputStream os = new FileOutputStream(destPath);
        photo.compress(CompressFormat.JPEG, 95, os);
        os.close();
        ContentValues values = new ContentValues();
        values.put("_data", destPath.getAbsolutePath());
        values.put("title", "Repix");
        values.put("mime_type", "image/jpeg");
        Uri uri = getContentResolver().insert(Media.EXTERNAL_CONTENT_URI, values);
        try {
            saveExif(uri);
        } catch (IOException e) {
        }
        MediaScannerConnection.scanFile(this, new String[]{destPath.toString()}, null, new OnScanCompletedListener() {
            public void onScanCompleted(String path, Uri uri) {
            }
        });
        return uri.toString();
    }

    void download(Uri uri) throws IOException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        try {
            Bitmap oldbmp;
            InputStream in = openInputStream(uri);
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, options);
            in.close();
            in = openInputStream(uri);
            int w = options.outWidth;
            int h = options.outHeight;
            float scale = Math.max(((float) options.outWidth) / ((float) 2048), ((float) options.outHeight) / ((float) 2048));
            options.inJustDecodeBounds = DEBUG;
            options.inSampleSize = (int) Math.floor((double) scale);
            options.inPurgeable = DEBUG;
            Bitmap bmp = BitmapFactory.decodeStream(in, null, options);
            in.close();
            if (bmp != null) {
                w = bmp.getWidth();
                h = bmp.getHeight();
                scale = Math.max(((float) w) / ((float) 2048), ((float) h) / ((float) 2048));
            } else {
                w = bmp.getWidth();
                h = bmp.getHeight();
                scale = Math.max(((float) w) / ((float) 2048), ((float) h) / ((float) 2048));
            }
            if (scale < 0.7f || scale > 1.0f) {
                oldbmp = bmp;
                bmp = Bitmap.createScaledBitmap(bmp, (int) (((float) w) / scale), (int) (((float) h) / scale), true);
                oldbmp.recycle();
            }
            if (bmp != null) {
                String filename = getContentFilename(uri);
                if (filename != null) {
                    this.exif.readExif(filename);
                }
                int orientation = 0;
                try {
                    String strOrientation = this.exif.getAttribute("Orientation");
                    if (strOrientation != null) {
                        orientation = Integer.parseInt(strOrientation);
                    }
                } catch (NumberFormatException e) {
                    orientation = 0;
                }
                if (!(orientation == 0 || orientation == 1)) {
                    Matrix matrix = new Matrix();
                    oldbmp = bmp;
                    bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
                    oldbmp.recycle();
                }
                setCurrentPhoto(bmp);
                return;
            }
            alert("Failed to open image", null);
        } catch (FileNotFoundException e2) {
            downloadHttp(uri);
        } catch (RuntimeException e3) {
            alert("Failed to open image\n" + e3.toString(), null);
        }
    }

    private InputStream openInputStream(Uri uri) throws FileNotFoundException {
        if ("file".equals(uri.getScheme())) {
            return new FileInputStream(uri.getPath());
        }
        return getContentResolver().openInputStream(uri);
    }

    public void setCurrentPhoto(Bitmap bmp) {
        pendingBitmap = bmp;
        this.exif.reset();
        requestRender();
    }

    public void openPhoto(Bitmap bmp) {
        if (bmp != null) {
            pendingBitmap = bmp;
            this.exif.reset();
            System.gc();
            queueEvent(new Runnable() {
                public void run() {
                    GL2JNILib.clear(null, 0, 0);
                    RepixActivity.this.openEditor();
                }
            });
        } else {
            pendingBitmap = bmp;
            this.exif.reset();
            System.gc();
            queueEvent(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    }

    public void downloadHttp(Uri uri) throws IOException {
    }

    public void onDestroy() {
        super.onDestroy();
    }

    protected void onCreate(Bundle icicle) {
//        Log.d(TAG, "BOARD " + Build.BOARD);
//        Log.d(TAG, "FINGERPRINT " + Build.FINGERPRINT);
//        Log.d(TAG, "MANUFACTURER " + Build.MANUFACTURER);
//        Log.d(TAG, "PRODUCT " + Build.PRODUCT);
//        Log.d(TAG, "DEVICE " + Build.DEVICE);

        super.onCreate(icicle);
        setContentView(R.layout.main);
        new MyAsyncTask().execute();

        if(Build.VERSION.SDK_INT>=24){
            try{
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        dialogExit = new Dialog(this, R.style.WideDialog);
        dialogExit.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        dialogExit.setContentView(R.layout.activity_exit);
        dialogExit.setCancelable(false);
        this.yes = (TextView) dialogExit.findViewById(R.id.btnYes);
        this.no = (TextView) dialogExit.findViewById(R.id.btnNo);
        this.yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogExit.dismiss();
                finish();
            }
        });
        this.no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogExit.dismiss();
            }
        });
        recyclerViewAds = dialogExit.findViewById(R.id.recycler_ads);
        recyclerViewAds.setLayoutManager(new GridLayoutManager(this, 3));
//        recyclerViewAds.setAdapter(new AdsAdapter(this));

        linearLayout = (LinearLayout) findViewById(R.id.main);
        if (Ads.isInternetOn(this)) {
            linearLayout.addView(FBAds.GetFbAdView(this,10), 1);
            ((LinearLayout)dialogExit.findViewById(R.id.lad)).addView(FBAds.GetAdViewMedium(this));
        }
        mHandler = new Handler();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startRepeatingTask();
            }
        }, mInterval);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_stripes);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.openDrawer,
                R.string.closeDrawer);
        actionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("onClick", "check");
                toggle();
            }
        });
        instance = this;
        glView = (GL2JNIView) findViewById(R.id.repix_gl);
        if (!handleSendImageIntent(getIntent())) {
            incrementStartCount();
            setCurrentPhoto(BitmapFactory.decodeStream(getClass().getResourceAsStream(DEFAULT_IMAGES[getStartCount() % DEFAULT_IMAGES.length])));
        }
        getWindow().setSoftInputMode(3);
        Configuration config = getResources().getConfiguration();
        boolean isNaturalPortrait = (GL2JNILib.getDeviceType() == 0 || GL2JNILib.getDeviceType() == 3) ? true : DEBUG;
        if (isNaturalPortrait) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setUpNavigationView();
    }

    private void initCategory() {
        if (RepixActivity.categories != null) {
            final Menu menu = this.navigationView.getMenu();
            for (int i = 0; i < RepixActivity.categories.size(); i++) {
                Category category = RepixActivity.categories.get(i);
                final MenuItem add = menu.add(R.id.main, i, 3, category.getTitle());
                Glide.with(this).load(Globle.GoogleDrive + category.getIcon()).asBitmap().into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        Drawable d = new BitmapDrawable(getResources(), resource);
                        add.setIcon(d);
                    }
                });
                add.setTitleCondensed(category.getLink());
            }
        }

    }

    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }

    private void setUpNavigationView() {

        navigationView.bringToFront();

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

               if (RepixActivity.this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    RepixActivity.this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST);
                    return true;
                }

                Log.d("onclick", "yes");
                switch (item.getItemId()) {
                    case R.id.nav_editor:

                        drawer.closeDrawer(Gravity.START);
                        openEditor();

                        break;
                    case R.id.nav_camera:

                        drawer.closeDrawer(Gravity.START);
                        openCamera();

                        break;
                    case R.id.nav_photos:

                        drawer.closeDrawer(Gravity.START);
                        openGallery();

                        break;
                    case R.id.nav_collection:

                        FBAds.showFbInterstitial(RepixActivity.this);
                        drawer.closeDrawer(Gravity.START);
                        startActivity(new Intent(RepixActivity.this, CollectionActivity.class));

                        break;
                    case R.id.nav_guide:

                        drawer.closeDrawer(Gravity.START);

                        break;
                    case R.id.nav_policy:

                        drawer.closeDrawer(Gravity.START);
                        Intent i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse(getString(R.string.privacylink)));
                        startActivity(i);

                        break;
                    case R.id.nav_rateus:

                        drawer.closeDrawer(Gravity.START);
                        onRateUs();

                        break;
                    case R.id.nav_share:

                        drawer.closeDrawer(Gravity.START);
                        onShare();

                        break;

                    case R.id.nav_privacy:

                        openWeb(getString(R.string.privacylink));
                        drawer.closeDrawer(Gravity.START);
                        break;

                    default:

                        drawer.closeDrawer(Gravity.START);
                        try {
                            startActivity(new Intent("android.intent.action.VIEW", Uri.parse(Globle.app_link + RepixActivity.categories.get(item.getItemId()).getLink())));
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(RepixActivity.this, "You don't have Google Play installed", Toast.LENGTH_SHORT).show();
                        }

                        break;

                }
                return true;
            }
        });

    }

    private void onRateUs() {

        Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName()));
        startActivity(rateIntent);

    }

    private void onShare() {

        shareDrawable(this, R.drawable.icon, "Header");

    }

    public void shareDrawable(Context context, int resourceId, String fileName) {
        try {
            File outputFile = new File(context.getCacheDir(), fileName + ".png");
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId);
            FileOutputStream outPutStream = new FileOutputStream(outputFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outPutStream);
            outPutStream.flush();
            outPutStream.close();
            outputFile.setReadable(true, false);
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(outputFile));
            shareIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=" + getPackageName());
            shareIntent.setType("image/png");
            context.startActivity(shareIntent);

        } catch (Exception e) {
            Toast.makeText(context, getString(R.string.share_google_plus_not_installed), Toast.LENGTH_LONG).show();
        }
    }

    private void toggle() {
        if (drawer.isDrawerVisible(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            drawer.openDrawer(GravityCompat.START);
        }
    }

    private void disableMenuKey() {
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, DEBUG);
            }
        } catch (Exception e) {
        }
    }

    protected void onNewIntent(Intent intent) {
        handleSendImageIntent(intent);
    }

    private boolean handleSendImageIntent(Intent intent) {
        String action = intent.getAction();
        String type = intent.getType();
        if ("android.intent.action.SEND".equals(action) && type != null && type.startsWith("image/")) {
            Uri imageUri = (Uri) intent.getParcelableExtra("android.intent.extra.STREAM");
            if (imageUri != null) {
                try {
                    download(imageUri);
                    return true;
                } catch (IOException e) {
                    alert(e.getMessage(), null);
                }
            } else {
                alert("No image available", null);
            }
        }
        return DEBUG;
    }

    private int getStartCount() {
        return getPreferences().getInt("count", 0);
    }

    private void incrementStartCount() {
        SharedPreferences settings = getPreferences();
        Editor editor = settings.edit();
        int count = settings.getInt("count", -1);
        if (count < 0) {
            count = 0;
        }
        editor.putInt("count", count + 1);
        editor.remove("android.test.purchased");
        editor.commit();
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && this.glViewHack) {
            this.glViewHack = DEBUG;
            glView.setVisibility(View.VISIBLE);
        }
    }

    protected void onPause() {
        this.glViewHack = true;
        glView.setVisibility(View.GONE);
        cancelAutoRestore();
        super.onPause();
    }

    protected void onResume() {
        super.onResume();
        glView.onResume();
    }

    public void requestRender() {
        runOnUiThread(new Runnable() {
            public void run() {
                RepixActivity.glView.requestRender();
            }
        });
    }

    public void performHapticFeedback(final int feedbackConstant) {
        runOnUiThread(new Runnable() {
            public void run() {
                //RepixActivity.this.shareDrawer.performHapticFeedback(feedbackConstant);
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RC_GALLERY /*40001*/:
            case RC_CAMERA /*40002*/:
                onRequestGalleryResult(resultCode, data);
                return;
            default:
                return;
        }
    }

    private void onRequestGalleryResult(int resultCode, Intent data) {
        if (resultCode == -1) {
            Uri uri = null;
            if (data != null) {
                uri = data.getData();
                if (uri == null && data.getExtras() != null) {
                    Uri uri2 = (Uri) data.getParcelableExtra("android.intent.extra.STREAM");
                }
            }
            if (uri == null) {
                uri = Uri.fromFile(getCameraFile());
            }
            try {
                download(uri);
                openEditor();
            } catch (IOException e) {
            }
        }
    }

    void openHelp() {
        hideToolbarButtons();
        //  setHeadingTitle((int) R.string.heading_help);
        //  getActionBar().setDisplayHomeAsUpEnabled(true);
        openWeb(URL_REPIX_HELP);
    }

    void openFeed() {
        hideToolbarButtons();
        //  setHeadingTitle((int) R.string.heading_starters);
        //  getActionBar().setDisplayHomeAsUpEnabled(true);
        openWeb(URL_REPIX_FEED);
    }

    void setHeadingTitle(int resTitle) {
        setHeadingTitle(getResources().getString(resTitle));
    }

    void setHeadingTitle(final String title) {
        runOnUiThread(new Runnable() {
            public void run() {
              /*  ActionBar actionBar = RepixActivity.this.getActionBar();
                if (actionBar != null) {
                    if ("Repix".equals(title)) {
                        actionBar.setDisplayShowTitleEnabled(RepixActivity.DEBUG);
                    } else {
                        actionBar.setDisplayShowTitleEnabled(true);
                    }
                    actionBar.setTitle(title);
                }*/
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public boolean onOptionsItemSelected(MenuItem item) {
        if (this.shareInfo == null || !this.shareInfo.containsKey(item)) {
            switch (item.getItemId()) {
                case android.R.id.home:
                    toggle();

                    break;

                case R.id.editor_reset /*2131099666*/:
                    Platform.platformCommand("reset_to_original");
                    return true;
                case R.id.editor_undo /*2131099667*/:
                    //  this.shareDrawer.performHapticFeedback(1);
                    queueEvent(new Runnable() {
                        public void run() {
                            GL2JNILib.undo();
                            RepixActivity.glView.requestRender();
                        }
                    });
                    return true;
                case R.id.editor_redo /*2131099668*/:
                    //  this.shareDrawer.performHapticFeedback(1);
                    queueEvent(new Runnable() {
                        public void run() {
                            GL2JNILib.redo();
                            RepixActivity.glView.requestRender();
                        }
                    });
                    return true;
                case R.id.share_gallery /*2131099670*/:
                    //this.shareDrawer.performHapticFeedback(1);
                    if (RepixActivity.this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        RepixActivity.this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST);
                        return true;
                    }
                    sharePhoto(null);
                    return true;
                case R.id.editor_store_close /*2131099672*/:
                    //  this.shareDrawer.performHapticFeedback(1);
                    closeStore();
                    return true;
                default:
                    return DEBUG;
            }
        }

        return super.onOptionsItemSelected(item);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        int i;
        getMenuInflater().inflate(R.menu.menu, menu);
        for (i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            if (item.getItemId() == 0) {
                item.setVisible(DEBUG);
            }
        }
        this.menuReset = menu.findItem(R.id.editor_reset);
        this.menuUndo = menu.findItem(R.id.editor_undo);
        this.menuRedo = menu.findItem(R.id.editor_redo);
        this.menuStoreClose = menu.findItem(R.id.editor_store_close);
        // SubMenu shareSubMenu = this.menuSave.getSubMenu();
        // SubMenu shareSubMenu2 = this.menuShareMore.getSubMenu();
        this.shareInfo = new HashMap();

        ImageButton ib = new ImageButton(this);
        ib.setImageDrawable(this.menuUndo.getIcon());
        ib.setBackgroundResource(0);
        this.menuUndo.setActionView(ib);
        this.menuUndo.getActionView().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // RepixActivity.this.shareDrawer.performHapticFeedback(1);
                RepixActivity.this.queueEvent(new Runnable() {
                    public void run() {
                        GL2JNILib.undo();
                        RepixActivity.glView.requestRender();
                    }
                });
            }
        });
        this.menuUndo.getActionView().setOnLongClickListener(new OnLongClickListener() {
            public boolean onLongClick(View view) {
                Platform.platformCommand("reset_to_original");
                return true;
            }
        });
        setRedoEnabled(DEBUG);
        return true;
    }

    public void hideToolbarButtons() {
        this.menuReset.setVisible(DEBUG);
        this.menuUndo.setVisible(DEBUG);
        this.menuRedo.setVisible(DEBUG);
        this.menuStoreClose.setVisible(DEBUG);
        this.menuSave.setVisible(DEBUG);
    }

    public void setToolbar(final String tag) {
        this.currentToolbar = tag;
        if (this.menuUndo != null) {
            runOnUiThread(new Runnable() {
                public void run() {
                    boolean z = true;
                    boolean brushes = tag.equals("brushes");
                    RepixActivity.this.menuUndo.setVisible(brushes);
                    RepixActivity.this.menuRedo.setVisible(brushes);
                    boolean store = tag.startsWith("store");
                    RepixActivity.this.menuStoreClose.setVisible(store);
                    RepixActivity.this.menuReset.setVisible(!store ? true : RepixActivity.DEBUG);
                    if (!store) {
                        RepixActivity.this.setHeadingTitle((int) R.string.app_name);
                    }
                }
            });
        }
    }

    public void openStore() {
        queueEvent(new Runnable() {
            public void run() {
                RepixActivity.this.openEditor();
                GL2JNILib.openStore();
                RepixActivity.glView.requestRender();
            }
        });
    }

    public void closeStore() {
        runOnUiThread(new Runnable() {
            public void run() {
                RepixActivity.this.setHeadingTitle((int) R.string.app_name);
            }
        });
        queueEvent(new Runnable() {
            public void run() {
                GL2JNILib.closeStore();
                RepixActivity.glView.requestRender();
            }
        });
    }

    public void openEditor() {
        runOnUiThread(new Runnable() {
            public void run() {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                RepixActivity.this.setHeadingTitle((int) R.string.app_name);
                if (RepixActivity.this.currentToolbar != null) {
                    RepixActivity.this.setToolbar(RepixActivity.this.currentToolbar);
                }
                RepixActivity.glView.setVisibility(View.VISIBLE);
                RepixActivity.this.requestRender();
            }
        });
    }

    private void showWhatsNewIfVersionChanged() {
        cancelAutoRestore();
        postAutoRestore();
    }

    public void openWeb(String url) {

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }

    public void onBackPressed() {

        if (this.drawer.isDrawerOpen(Gravity.START)) {
            this.drawer.closeDrawer(Gravity.START);
        } else if (Ads.isInternetOn(this)) {

            dialogExit.show();

        } else if (isBackPressed == 0) {
            isBackPressed++;
            Toast.makeText(this, getString(R.string.pressAgainToExit), Toast.LENGTH_SHORT).show();
        } else if (isBackPressed >= 1) {
            super.onBackPressed();
        }
//        else if (this.webviewlayout.getVisibility() == 0) {
//            openEditor();
//        }
        else {
            queueEvent(new Runnable() {
                public void run() {
                    if (GL2JNILib.closeStore()) {
                        RepixActivity.glView.requestRender();
                        RepixActivity.this.openEditor();
                        return;
                    }
                    GL2JNILib.didReceiveMemoryWarning();
                    RepixActivity.this.moveTaskToBack(true);
                }
            });
        }
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode != 82) {
            return super.onKeyUp(keyCode, event);
        }
        toggleMenu();
        return true;
    }

    void hideSplash() {

    }

    void openGallery() {
        Intent intent = new Intent("android.intent.action.PICK", Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        try {
            startActivityForResult(intent, RC_GALLERY);
        } catch (ActivityNotFoundException e) {
            alert("No activity found that can handle intent " + intent.getAction(), null);
        }
    }

    private File getCameraFile() {
        return new File(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Repix"), "camera.jpg");
    }

    void openCamera() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", Uri.fromFile(getCameraFile()));
        try {
            startActivityForResult(intent, RC_CAMERA);
        } catch (ActivityNotFoundException e) {
            alert("No activity found that can handle intent " + intent.getAction(), null);
        }
    }

    void toggleMenu() {
        runOnUiThread(new Runnable() {
            public void run() {
                DisplayMetrics metrics = new DisplayMetrics();
                RepixActivity.getInstance().getWindowManager().getDefaultDisplay().getMetrics(metrics);

            }
        });
    }

    void toggleShareMenu() {
        runOnUiThread(new Runnable() {
            public void run() {

            }
        });
    }

    protected void onStart() {
        super.onStart();
    }

    protected void onStop() {
        super.onStop();
        stopRepeatingTask();
    }

    private void postAutoRestore() {
        this.restoreTimer.postDelayed(this.restoreTimerRunnable, 500);
    }

    private void cancelAutoRestore() {
        this.restoreTimer.removeCallbacks(this.restoreTimerRunnable);
    }

    public SharedPreferences getPreferences() {
        return getPreferences(0);
    }

    public void setRedoEnabled(final boolean state) {
        if (state != this.redoState && this.menuRedo != null) {
            this.redoState = state;
            runOnUiThread(new Runnable() {
                public void run() {
                    RepixActivity.this.menuRedo.setEnabled(state);
                }
            });
        }
    }

    void showGuide() {

    }

    private class MyAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL url = new URL(Globle.GoogleDrive + getString(R.string.catergoryLink));

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                resultJson = buffer.toString();

                Log.d("data", resultJson);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Type type = new TypeToken<ArrayList<Category>>() {
            }.getType();
            categories = new Gson().fromJson(resultJson, type);
            initCategory();

        }
    }

}
