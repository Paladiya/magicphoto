package it.repix.android;

import android.util.Log;

public class DBLog {
    public static boolean LOG = false;

    public static void LOGI(String tag, String string) {
        if (LOG) {
            Log.i(tag, string);
        }
    }

    public static void LOGE(String tag, String string) {
        if (LOG) {
            Log.e(tag, string);
        }
    }

    public static void LOGD(String tag, String string) {
        if (LOG) {
            Log.d(tag, string);
        }
    }

    public static void LOGV(String tag, String string) {
        if (LOG) {
            Log.v(tag, string);
        }
    }

    public static void LOGW(String tag, String string) {
        if (LOG) {
            Log.w(tag, string);
        }
    }

    public static void setDebug(boolean b) {
        LOG = b;
    }
}
