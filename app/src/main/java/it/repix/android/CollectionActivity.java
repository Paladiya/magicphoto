package it.repix.android;
import android.os.Build;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.File;

import it.repix.android.Adapter.CreationAdapter;

public class CollectionActivity extends AppCompatActivity {

    private ImageView noImage;
    private CreationAdapter galleryAdapter;
    private GridView lstList;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection);

        linearLayout = (LinearLayout)findViewById(R.id.main);
        noImage = (ImageView) findViewById(R.id.novideoimg);
        lstList = (GridView) findViewById(R.id.lstList);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (Ads.isInternetOn(this))
        {
            linearLayout.addView(FBAds.GetFbAdView(this,10));
            linearLayout.addView(FBAds.GetFbAdView(this,10));
        }

        getImages();
        this.galleryAdapter = new CreationAdapter(this, Globle.IMAGEALLARY);
        this.lstList.setAdapter(this.galleryAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Globle.IMAGEALLARY.size() <= 0) {
            this.noImage.setVisibility(View.VISIBLE);
            this.lstList.setVisibility(View.GONE);
        } else {
            this.noImage.setVisibility(View.GONE);
            this.lstList.setVisibility(View.VISIBLE);
        }
    }

    private void getImages() {
        if (Build.VERSION.SDK_INT < 23) {
            fetchImage();
        } else if (checkSelfPermission("android.permission.READ_EXTERNAL_STORAGE") == getPackageManager().PERMISSION_GRANTED) {
            fetchImage();
        } else if (checkSelfPermission("android.permission.READ_EXTERNAL_STORAGE") != getPackageManager().PERMISSION_GRANTED) {
            requestPermissions(new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, 5);
        }
    }

    private void fetchImage() {
        Globle.IMAGEALLARY.clear();
        Globle.listAllImages(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Repix"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
