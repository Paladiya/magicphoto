package it.repix.android;

import android.app.Application;

import com.facebook.ads.AudienceNetworkAds;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AudienceNetworkAds.initialize(this);
        FBAds.LoadFbInterstitialAd(this);
        FBAds.LoadGoogleInterstitialAds(this);
    }
}
