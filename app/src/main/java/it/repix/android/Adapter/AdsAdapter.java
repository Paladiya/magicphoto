package it.repix.android.Adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import it.repix.android.Globle;
import it.repix.android.R;
import it.repix.android.RepixActivity;

public class AdsAdapter extends RecyclerView.Adapter<AdsAdapter.MyViewHolder> {

    Context context;

    public AdsAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_ads_layout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.textView.setText(RepixActivity.categories.get(position).getTitle());
        Glide.with(context).load(Globle.GoogleDrive+RepixActivity.categories.get(position).getIcon()).asBitmap().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                holder.imageView.setImageBitmap(resource);
            }
        });
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(Globle.app_link +RepixActivity.categories.get(position).getLink())));
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(context, "You don't have Google Play installed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return RepixActivity.categories.size();
    }

    class  MyViewHolder extends RecyclerView.ViewHolder{

        LinearLayout linearLayout;
        ImageView imageView;
        TextView textView;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView)itemView.findViewById(R.id.iv_icon);
            textView = (TextView)itemView.findViewById(R.id.tv_name);
            linearLayout = (LinearLayout)itemView.findViewById(R.id.lin_ads);
        }
    }
}
