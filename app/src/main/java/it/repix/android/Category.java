package it.repix.android;

public class Category {

    String icon,link,title;
    int id;

    public Category() {

    }

    public Category(String icon, int id, String link, String title) {
        this.icon = icon;
        this.id = id;
        this.link = link;
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
