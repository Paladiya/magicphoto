package it.repix.android;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;

public class TestView extends GLSurfaceView {
    private static int EGL_CONTEXT_CLIENT_VERSION = 12440;
    public static final String TAG = "repix";
    EGLContext context = null;
    MyGLRenderView renderer = new MyGLRenderView();

    class ContextFactory implements EGLContextFactory {
        ContextFactory() {
        }

        public EGLContext createContext(EGL10 egl, EGLDisplay display, EGLConfig eglConfig) {
            if (TestView.this.context == null) {
                Log.w(TestView.TAG, "creating OpenGL ES 2.0 context");
                int[] attrib_list = new int[]{TestView.EGL_CONTEXT_CLIENT_VERSION, 2, 12344};
                TestView.this.context = egl.eglCreateContext(display, eglConfig, EGL10.EGL_NO_CONTEXT, attrib_list);
            }
            Log.w(TestView.TAG, "createContext = " + TestView.this.context);
            return TestView.this.context;
        }

        public void destroyContext(EGL10 egl, EGLDisplay display, EGLContext context) {
            Log.d("TAG", "destroyContext " + context);
        }
    }

    class MyGLRenderView implements Renderer {
        MyGLRenderView() {
        }

        public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        }

        public void onSurfaceChanged(GL10 gl, int w, int h) {
            gl.glViewport(0, 0, w, h);
        }

        public void onDrawFrame(GL10 gl) {
            gl.glClearColor(0.0f, (float) Math.random(), 0.0f, 0.5f);
            gl.glClear(16640);
        }
    }

    public TestView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setEGLContextClientVersion(2);
        setRenderer(this.renderer);
    }
}
