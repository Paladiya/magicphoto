
package it.repix.android;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Platform {
    public static final boolean DEBUG = false;
    public static final String TAG = "repix";

    public static void log(String msg) {
    }

    public static Bitmap loadBitmap(String path) throws IOException {
        return BitmapFactory.decodeStream(Platform.class.getResourceAsStream("/" + path));
    }

    public static boolean isProductPurchased(String product) {
        SharedPreferences prefs = RepixActivity.getInstance().getPreferences();
      /*  boolean b = PurchaseManager.getInstance().isProductPurchased("it.repix.brushpack.all2", prefs.getString("it.repix.brushpack.all2", null));
        if (b) {
            return b;
        }
        b = PurchaseManager.getInstance().isProductPurchased(product, prefs.getString(product, null));
        if (b) {
            return b;
        }*/
        return true;
    }

    public static void requestProductDetails(final String[] products, final int callback, final int context) {
      /*  new Thread(new Runnable() {
            public void run() {
                PurchaseManager.getInstance().requestProductDetails(products, callback, context);
            }
        }).start();*/
    }

    public static void platformCommand(String command) {
        if (command.startsWith("toolbartitle")) {
            RepixActivity.getInstance().setHeadingTitle(command.substring(13));
        } else if (command.startsWith("toolbar")) {
            RepixActivity.getInstance().setToolbar(command.substring(8));
        } else {
            if (command.startsWith("button_long_pressed:")) {
                RepixActivity.getInstance().performHapticFeedback(0);
            }
            if (command.startsWith("toggle_clicked:") || command.startsWith("button_clicked:") || command.startsWith("brush:") || command.startsWith("frame:") || command.startsWith("preset:") || command.startsWith("crop:")) {
                RepixActivity.getInstance().performHapticFeedback(1);
            }
          /*  if (command.startsWith("buy:")) {
                PurchaseManager.getInstance().buy(command.substring(4));
            }
            if (command.equals("restore")) {
                PurchaseManager.getInstance().restorePurchases(DEBUG);
            }*/
            if (command.equals("camera")) {
                RepixActivity.getInstance().openCamera();
            }
            if (command.equals("photoroll")) {
                RepixActivity.getInstance().openGallery();
            }
            if (command.equals("editor_back")) {
                RepixActivity.getInstance().toggleMenu();
            }
            if (command.equals("editor_done")) {
                RepixActivity.getInstance().toggleShareMenu();
            }
            if (command.equals("reset_to_original")) {
                RepixActivity.getInstance().confirm("Reset to original?", new OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RepixActivity.getInstance().queueEvent(new Runnable() {
                            public void run() {
                                GL2JNILib.resetToOriginal();
                                RepixActivity.glView.requestRender();
                            }
                        });
                    }
                });
            }
        }
    }

    public static byte[] loadBinaryFile(String path) throws IOException {
        InputStream is = Platform.class.getResourceAsStream("/" + path);
        if (is == null) {
            return null;
        }
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] buf = new byte[4096];
            while (true) {
                int c = is.read(buf);
                if (c <= 0) {
                    break;
                }
                bos.write(buf, 0, c);
            }
            byte[] toByteArray = bos.toByteArray();
            return toByteArray;
        } finally {
            is.close();
        }
    }
}
